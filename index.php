<?php 
require_once('./animal.php');
require_once('./Frog.php');
require_once('./Ape.php');


$sheep = new Animal("shaun");
echo "name : ";
echo $sheep->name; // "shaun"
echo "<br/>legs : ";
echo $sheep->legs; // 4
echo "<br/>cold blooded : ";
echo $sheep->cold_blooded; // "no"
echo "<hr/>";

$kodok = new Frog("buduk");
echo "name : ";
echo $kodok->name; 
echo "<br/>legs : ";
echo $kodok->legs; 
echo "<br/>cold blooded : ";
echo $kodok->cold_blooded; 
echo "<br/>Jump : ";
echo $kodok->jump(); 
echo "<hr/>";

$sungokong = new Ape("kera sakti");
echo "name : ";
echo $sungokong->name; 
echo "<br/>legs : ";
echo $sungokong->legs; 
echo "<br/>cold blooded : ";
echo $sungokong->cold_blooded; 
echo "<br/>Yell : ";
$sungokong->yell() // "Auooo"
?>